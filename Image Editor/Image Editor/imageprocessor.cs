﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using AForge.Imaging.Filters;


namespace Image_Editor
{
   public static class imageprocessor

    {
       public static Bitmap Brightness(this Bitmap bitmap, double brightnessValue)
        {
            AForge.Imaging.Filters.BrightnessCorrection bc = new AForge.Imaging.Filters.BrightnessCorrection(brightnessValue);
            return bc.Apply(bitmap);
        }

       public static Bitmap HueModifier(this Bitmap bitmap, int hue)
       {
          AForge.Imaging.Filters.HueModifier huemodifier = new AForge.Imaging.Filters.HueModifier(hue);
          return huemodifier.Apply(bitmap);

       }
       public static Bitmap Contrast(this Bitmap bitmap, double contrastValue)
       {
           AForge.Imaging.Filters.ContrastCorrection contrastcorrection = new AForge.Imaging.Filters.ContrastCorrection(contrastValue);
           return contrastcorrection.Apply(bitmap);

       }
       public static Bitmap Saturation(this Bitmap bitmap, double saturationValue)
       {
           AForge.Imaging.Filters.SaturationCorrection saturationcorrection = new AForge.Imaging.Filters.SaturationCorrection(saturationValue);
           return saturationcorrection.Apply(bitmap);

       }
     
     
     
    }
}
