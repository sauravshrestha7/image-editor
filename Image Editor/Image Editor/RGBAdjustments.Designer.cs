﻿namespace Image_Editor
{
    partial class RGBAdjustments
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelrgb = new System.Windows.Forms.Panel();
            this.panelBrightness = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.brightnessTrackBar = new System.Windows.Forms.TrackBar();
            this.brightnessBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panelrgb.SuspendLayout();
            this.panelBrightness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // panelrgb
            // 
            this.panelrgb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelrgb.Controls.Add(this.panelBrightness);
            this.panelrgb.Location = new System.Drawing.Point(8, 8);
            this.panelrgb.Name = "panelrgb";
            this.panelrgb.Size = new System.Drawing.Size(439, 679);
            this.panelrgb.TabIndex = 6;
            this.panelrgb.Paint += new System.Windows.Forms.PaintEventHandler(this.panelrgb_Paint);
            // 
            // panelBrightness
            // 
            this.panelBrightness.Controls.Add(this.cancelButton);
            this.panelBrightness.Controls.Add(this.okButton);
            this.panelBrightness.Controls.Add(this.brightnessTrackBar);
            this.panelBrightness.Controls.Add(this.brightnessBox);
            this.panelBrightness.Controls.Add(this.label6);
            this.panelBrightness.Location = new System.Drawing.Point(3, 20);
            this.panelBrightness.Name = "panelBrightness";
            this.panelBrightness.Size = new System.Drawing.Size(343, 616);
            this.panelBrightness.TabIndex = 6;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.White;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(147, 194);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 28;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.White;
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(57, 194);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 27;
            this.okButton.Text = "&Ok";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // brightnessTrackBar
            // 
            this.brightnessTrackBar.Location = new System.Drawing.Point(7, 91);
            this.brightnessTrackBar.Maximum = 1000;
            this.brightnessTrackBar.Minimum = -1000;
            this.brightnessTrackBar.Name = "brightnessTrackBar";
            this.brightnessTrackBar.Size = new System.Drawing.Size(250, 45);
            this.brightnessTrackBar.TabIndex = 26;
            // 
            // brightnessBox
            // 
            this.brightnessBox.Location = new System.Drawing.Point(161, 60);
            this.brightnessBox.Name = "brightnessBox";
            this.brightnessBox.Size = new System.Drawing.Size(50, 20);
            this.brightnessBox.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(7, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 16);
            this.label6.TabIndex = 24;
            this.label6.Text = "Adjust brightness by:";
            // 
            // RGBAdjustments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelrgb);
            this.Name = "RGBAdjustments";
            this.Size = new System.Drawing.Size(269, 687);
            this.panelrgb.ResumeLayout(false);
            this.panelBrightness.ResumeLayout(false);
            this.panelBrightness.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelrgb;
        private System.Windows.Forms.Panel panelBrightness;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TrackBar brightnessTrackBar;
        private System.Windows.Forms.TextBox brightnessBox;
        private System.Windows.Forms.Label label6;
    }
}
