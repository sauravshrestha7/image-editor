﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;
using AForge.Imaging.Filters;
using System.Threading;


namespace Image_Editor
{
    public partial class Main : Form
    {
        Stack<Image> UChanges = new Stack<Image>(5);
        Stack<Image> RChanges = new Stack<Image>(5);

        public Main()
        {
            
            InitializeComponent();
         
        }
        //boolean to check whether the image is already opened 
        System.Drawing.Image file;
        public Boolean opened = false;
        public Boolean Makeselection = false;

        //declaring boolean to fit the image to the screen
        public Boolean isFitsoscreen = false;

        //declaring bitmap to store the data of the picture
        Bitmap picture;
        //get the size of the image 
        Size picture_size;

        //for zoom 
        int zoom = 1;
        int limit = 128; 

        public void UCAdd(Image img)
        {
            UChanges.Push(img);
            //ndoToolStripMenuItem.Enabled = true;
        }

        public void RCAdd(Image img)
        {
            RChanges.Push(img);
            //rToolStripMenuItem.Enabled = true;
        }
     
      
        public void Open()
        {

            DialogResult dr = openFileDialog1.ShowDialog();

            if (dr == DialogResult.OK)
            {
                file = System.Drawing.Image.FromFile(openFileDialog1.FileName);
                pictureBox1.Image = file;
                opened = true;
                isFittoscreen();
              

            }


        }

       public  void reload()
        {
            if (!opened)
            {
                MessageBox.Show("Error: Please open an image first");
            }

            else
            {

                //pictureBox1.Width = 720;
                //pictureBox1.Height = 480;

                pictureBox1.Image = System.Drawing.Image.FromFile(openFileDialog1.FileName);
                file = pictureBox1.Image;
                opened = true;
            }
        }

       public void isFittoscreen()
       {
           if (isFitsoscreen)
           {
               pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
           }
       }
        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void btnbrightness_Click(object sender, EventArgs e)
        {

            panelBrightness.BringToFront();
        }

        private void btnaddjustments_Click(object sender, EventArgs e)
        {
            panelrgb.BringToFront();
            
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (opened)
            {
                SaveFileDialog sfd = new SaveFileDialog();

                sfd.Filter = "Images|*.png;*.bmp;*.jpg";
                ImageFormat format = ImageFormat.Png;

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string ext = Path.GetExtension(sfd.FileName);

                    switch (ext)
                    {
                        case ".jpg":
                            format = ImageFormat.Jpeg;
                            break;

                        case ".bmp":
                            format = ImageFormat.Bmp;
                            break;
                    }

                    pictureBox1.Image.Save(sfd.FileName, format);
                }
            }


        }

      

        private void btneffect_Click(object sender, EventArgs e)
        {
            if (opened)
            {
                effectpanel.BringToFront();

                System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;
                btnsepia.Image = image.ToSepia();
                btnblacknwhite.Image = image.ToBlackAndWhite();
                btnpixellate.Image = image.ToPixalation();
                btninvert.Image = image.ToInvert();
                effectpanel.Show();
                

                
            }
            else
            {
                MessageBox.Show("Image not loaded");
            }
        }

        private void bunifuImageButton7_Click(object sender, EventArgs e)
        {
            
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                isFitsoscreen = true;
            
        }

        private void btnZoomIN_Click(object sender, EventArgs e)
        {
            picture = new Bitmap(pictureBox1.Image);
            
            picture_size = picture.Size;
          
            if (zoom < limit)
            {
                zoom = zoom * 2;
            }
            pictureBox1.Size = new Size(zoom * picture_size.Width, zoom * picture_size.Height);
            pictureBox1.Invalidate();
            isFitsoscreen = false;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnsepia_Click(object sender, EventArgs e)
        {
            UCAdd(pictureBox1.Image);
            System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;
            pictureBox1.Image = image.ToSepia();
            RChanges.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panelBrightness.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panelrgb.BringToFront();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            effectpanel.BringToFront();
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            if (zoom > 0)
            {
                zoom = zoom / 2;

            }

            pictureBox1.Size = new Size(zoom * picture_size.Width, zoom * picture_size.Height);
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            if (picture != null)
            {
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                e.Graphics.DrawImage(picture, 0, 0, pictureBox1.Width * zoom, pictureBox1.Height * zoom);
            }
        }

        private void btnblacknwhite_Click(object sender, EventArgs e)
        {
            UCAdd(pictureBox1.Image);
            System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;
            pictureBox1.Image = image.ToBlackAndWhite(); 
            RChanges.Clear();
        }

        private void btninvert_Click(object sender, EventArgs e)
        {
            Bitmap image = (Bitmap)pictureBox1.Image;
           
            
      
        }

        private void btnpixellate_Click(object sender, EventArgs e)
        {

            UCAdd(pictureBox1.Image);
            System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;
            pictureBox1.Image = image.ToPixalation();
            RChanges.Clear();
        }

       

        void hue()
        {





            if (!opened)
            {

            }
            else
            {
                float changered = trackbarred.Value * 0.1f;
                float changegreen = trackbargreen.Value * 0.1f;
                float changeblue = trackbarblue.Value * 0.1f;

                colorbalance cb = new colorbalance();
                Bitmap bmp = cb.RGB(file, changered, changegreen, changeblue);
                pictureBox1.Image = bmp;


            //    float[][] colormatrixelements =      // now creating the color matrix object to change the colors or apply  image filter on image
            //   {
            //        new float[]{1+changered, 0, 0, 0, 0},
            //new float[]{0, 1+changegreen, 0, 0, 0},
            //new float[]{0, 0, 1+changeblue, 0, 0},
            //new float[]{0, 0, 0, 1, 0},
            //new float[]{0, 0, 0, 0, 1}
            //    };

            //    //creating an object of imageattribute ia to change the attribute of images
            //    ColorMatrix cmPicture = new ColorMatrix(colormatrixelements);
            //    ImageAttributes ia = new ImageAttributes();

            //    ia.SetColorMatrix(cmPicture, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);           //pass the color matrix to imageattribute object ia


            //    Image img = file;                             // storing image into img variable of image type from picturebox1
            //    Bitmap bmpInverted = new Bitmap(img);   /* creating a bitmap of the height of imported picture in picturebox which consists of the pixel data for a graphics image
                 
            //                                            and its attributes. A Bitmap is an object used to work with images defined by pixel data.*/
            //    Graphics g = default(Graphics);
            //    g = Graphics.FromImage(bmpInverted);   /*create a new object of graphics named g, ; Create graphics object for alteration.
            //                                                Graphics newGraphics = Graphics.FromImage(imageFile); is the format of loading image into graphics for alteration*/

            //    g.DrawImage(img, new Rectangle(0, 0, img.Width+1, img.Height+1), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, ia);



            //    /*   g.drawimage(image, new rectangle(location of rectangle axix-x, location axis-y, width of rectangle, height of rectangle),
            //   location of image in rectangle x-axis, location of image in rectangle y-axis, width of image, height of image,
            //   format of graphics unit,provide the image attributes   */



            //    //Releases all resources used by this Graphics.

            //    pictureBox1.Image = bmpInverted;

            //    g.Dispose();



            }
        }

    

        private void trackbargreen_Scroll(object sender, EventArgs e)
        {
            lblgreen.Text = trackbarred.Value.ToString();
            UCAdd(pictureBox1.Image);
            hue();
            RChanges.Clear();
        }

        private void trackbarred_Scroll_1(object sender, EventArgs e)
        {
            lblredvalue.Text = trackbarred.Value.ToString();
            UCAdd(pictureBox1.Image);
            hue();
            RChanges.Clear();
        }

        private void brightnessTrackBar_Scroll(object sender, EventArgs e)
        {
            brightnessBox.Text = Convert.ToString((double)brightnessTrackBar.Value / 1000);
            lblshwbrightnessper.Text = Math.Round(Convert.ToDouble(brightnessBox.Text) * 100) +" %";

        }

       

        private void trackbarblue_Scroll(object sender, EventArgs e)
        {
            lblblue.Text = trackbarred.Value.ToString();
            UCAdd(pictureBox1.Image);
            hue();
            RChanges.Clear();
        }

        private void btnBright_Click(object sender, EventArgs e)
        {
            if (!opened)
            {
                MessageBox.Show("Error: Please open an image first");
            }
            else
            {
                UCAdd(pictureBox1.Image);

                //System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;

                Image img = file;                             // storing image into img variable of image type from picturebox1
                Bitmap bmpBrightness = new Bitmap(img);

                pictureBox1.Image = bmpBrightness.Brightness(Convert.ToDouble(brightnessBox.Text));
                RChanges.Clear();
            }
        }

        private void btnREDO_Click(object sender, EventArgs e)
        {
            if (RChanges.Count != 0)
            {
                UCAdd(pictureBox1.Image);
                pictureBox1.Image = RChanges.Pop();
              
            }
        }

        private void BTNUNDO_Click(object sender, EventArgs e)
        {
            if (UChanges.Count != 0)
            {
                RCAdd(pictureBox1.Image);
                pictureBox1.Image = UChanges.Pop();

            }
        }

      

        private void huePicker1_ValuesChanged(object sender, EventArgs e)
        {
            hueBox.Text = huePicker1.Min.ToString();
        }

    
        private void btnapplyhue_Click(object sender, EventArgs e)
        {

            UCAdd(pictureBox1.Image);

            System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;

            pictureBox1.Image = image.HueModifier(Convert.ToInt16(hueBox.Text));

            RChanges.Clear();
        }

     
        private void brightnessTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            if (!opened)
            {
                MessageBox.Show("Error: Please open an image first");
            }
            else
            {
                UCAdd(pictureBox1.Image);

                //System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;

                Image img = file;                             // storing image into img variable of image type from picturebox1
                Bitmap bmpBrightness = new Bitmap(img);

                pictureBox1.Image = bmpBrightness.Brightness(Convert.ToDouble(brightnessBox.Text));
                RChanges.Clear();
            }
        }

        private void trackbarcontrast_Scroll(object sender, EventArgs e)
        {

            lblcontrastper.Text = Convert.ToString((double)trackbarcontrast.Value / 1000);
        }

        private void btnapplyContrast_Click(object sender, EventArgs e)
        {
            UCAdd(pictureBox1.Image);

            System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;

            pictureBox1.Image = image.Contrast(Convert.ToDouble(lblcontrastper.Text));

            RChanges.Clear();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            lblcontrastper.Text = trackbarcontrast.Value.ToString();
        }

        

        private void btnsaturation_Click(object sender, EventArgs e)
        {

            if (!opened)
            {
                MessageBox.Show("Error: Please open an image first");
            }
            else
            {
                UCAdd(pictureBox1.Image);

                //System.Drawing.Bitmap image = (Bitmap)pictureBox1.Image;

                Image img = file;                             // storing image into img variable of image type from picturebox1
                Bitmap bmpBrightness = new Bitmap(img);

                pictureBox1.Image = bmpBrightness.Saturation(Convert.ToDouble(txtsaturationvalue.Text));
                RChanges.Clear();
            }
        }

        private void trackbarsaturation_Scroll(object sender, EventArgs e)
        {
            txtsaturationvalue.Text = Convert.ToString((double)trackbarsaturation.Value / 1000);
            lblsaturationvalue.Text = Math.Round(Convert.ToDouble(txtsaturationvalue.Text) * 100).ToString() + " %";
        }

        private void btncontrast_Click(object sender, EventArgs e)
        {
            panelcontrast.BringToFront();
        }

  
        private void bunifuImageButton3_Click(object sender, EventArgs e)
        {
            //displaying th saturation adjustment panel 
            panelSaturation.BringToFront();
        }

        int cropX = 0;
        int cropY = 0;
        public Pen cropPen;
        int cropWidth;
        int cropHeight;
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (Makeselection)
            {

                try
                {
                    if (pictureBox1.Image == null)
                        return;


                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        pictureBox1.Refresh();
                        cropWidth = e.X - cropX;
                        cropHeight = e.Y - cropY;
                        pictureBox1.CreateGraphics().DrawRectangle(cropPen, cropX, cropY, cropWidth, cropHeight);
                    }



                }
                catch (Exception ex)
                {
                    //if (ex.Number == 5)
                    //    return;
                }
            }
        }

        private void btncrop_Click(object sender, EventArgs e)
        {
            Makeselection = true;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (Makeselection)
            {

                try
                {
                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        Cursor = Cursors.Cross;
                        cropX = e.X;
                        cropY = e.Y;

                        cropPen = new Pen(Color.Black, 1);
                        cropPen.DashStyle = DashStyle.DashDotDot;


                    }
                    pictureBox1.Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;

            try
            {
                if (cropWidth < 1)
                {
                    return;
                }
                Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
                //First we define a rectangle with the help of already calculated points
                Bitmap OriginalImage = new Bitmap(pictureBox1.Image, pictureBox1.Width, pictureBox1.Height);
                //Original image
                Bitmap _img = new Bitmap(cropWidth, cropHeight);
                // for cropinf image
                Graphics g = Graphics.FromImage(_img);
                // create graphics
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                //set image attributes
                g.DrawImage(OriginalImage, 0, 0, rect, GraphicsUnit.Pixel);

                pictureBox1.Image = _img;
               
                
            }
            catch (Exception ex)
            {
            }
        }

       
    }
}
