﻿namespace Image_Editor
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.btncontrast = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnsharpness = new Bunifu.Framework.UI.BunifuImageButton();
            this.btneffect = new Bunifu.Framework.UI.BunifuImageButton();
            this.btncrop = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnaddjustments = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnbrightness = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnsave = new Bunifu.Framework.UI.BunifuImageButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.btnZoomIN = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnZoomOut = new Bunifu.Framework.UI.BunifuImageButton();
            this.panelBrightness = new System.Windows.Forms.Panel();
            this.lblshwbrightnessper = new System.Windows.Forms.Label();
            this.btnBright = new Bunifu.Framework.UI.BunifuFlatButton();
            this.brightnessTrackBar = new System.Windows.Forms.TrackBar();
            this.brightnessBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.effectpanel = new System.Windows.Forms.Panel();
            this.bunifuImageButton10 = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnpixellate = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton13 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton12 = new Bunifu.Framework.UI.BunifuImageButton();
            this.btninvert = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnblacknwhite = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton11 = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnjitter = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnsepia = new Bunifu.Framework.UI.BunifuImageButton();
            this.panelrgb = new System.Windows.Forms.Panel();
            this.lblblue = new System.Windows.Forms.Label();
            this.lblgreen = new System.Windows.Forms.Label();
            this.lblredvalue = new System.Windows.Forms.Label();
            this.trackbarblue = new System.Windows.Forms.TrackBar();
            this.trackbargreen = new System.Windows.Forms.TrackBar();
            this.label13 = new System.Windows.Forms.Label();
            this.trackbarred = new System.Windows.Forms.TrackBar();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bunifuImageButton7 = new Bunifu.Framework.UI.BunifuImageButton();
            this.label16 = new System.Windows.Forms.Label();
            this.BTNUNDO = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnapplyhue = new Bunifu.Framework.UI.BunifuFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hueBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREDO = new System.Windows.Forms.Button();
            this.panelcontrast = new System.Windows.Forms.Panel();
            this.lblcontrastper = new System.Windows.Forms.Label();
            this.btnapplyContrast = new Bunifu.Framework.UI.BunifuFlatButton();
            this.trackbarcontrast = new System.Windows.Forms.TrackBar();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panelSaturation = new System.Windows.Forms.Panel();
            this.txtsaturationvalue = new System.Windows.Forms.TextBox();
            this.lblsaturationvalue = new System.Windows.Forms.Label();
            this.btnsaturation = new Bunifu.Framework.UI.BunifuFlatButton();
            this.trackbarsaturation = new System.Windows.Forms.TrackBar();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.huePicker1 = new Image_Editor.Control.HuePicker();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncontrast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsharpness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btneffect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncrop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnaddjustments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnbrightness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnZoomIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnZoomOut)).BeginInit();
            this.panelBrightness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).BeginInit();
            this.effectpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnpixellate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btninvert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnblacknwhite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnjitter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsepia)).BeginInit();
            this.panelrgb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarblue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbargreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton7)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelcontrast.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarcontrast)).BeginInit();
            this.panelSaturation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarsaturation)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(1772, 0);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(53, 33);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 0;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.label7);
            this.bunifuGradientPanel1.Controls.Add(this.label17);
            this.bunifuGradientPanel1.Controls.Add(this.label11);
            this.bunifuGradientPanel1.Controls.Add(this.label10);
            this.bunifuGradientPanel1.Controls.Add(this.label5);
            this.bunifuGradientPanel1.Controls.Add(this.label4);
            this.bunifuGradientPanel1.Controls.Add(this.label3);
            this.bunifuGradientPanel1.Controls.Add(this.label1);
            this.bunifuGradientPanel1.Controls.Add(this.bunifuImageButton3);
            this.bunifuGradientPanel1.Controls.Add(this.btncontrast);
            this.bunifuGradientPanel1.Controls.Add(this.btnsharpness);
            this.bunifuGradientPanel1.Controls.Add(this.btneffect);
            this.bunifuGradientPanel1.Controls.Add(this.btncrop);
            this.bunifuGradientPanel1.Controls.Add(this.btnaddjustments);
            this.bunifuGradientPanel1.Controls.Add(this.btnbrightness);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.DimGray;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(129, 851);
            this.bunifuGradientPanel1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "label7";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(24, 678);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 20);
            this.label17.TabIndex = 5;
            this.label17.Text = "Saturation";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(24, 583);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 20);
            this.label11.TabIndex = 5;
            this.label11.Text = "Contrast";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(24, 505);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 20);
            this.label10.TabIndex = 5;
            this.label10.Text = "Sharpen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(24, 416);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Effects";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(32, 327);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Crop";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(12, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Adjustment";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(12, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Brightness";
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton3.Image = global::Image_Editor.Properties.Resources.saturation;
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(28, 617);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(57, 52);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bunifuImageButton3.TabIndex = 4;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 10;
            this.bunifuImageButton3.Click += new System.EventHandler(this.bunifuImageButton3_Click);
            // 
            // btncontrast
            // 
            this.btncontrast.BackColor = System.Drawing.Color.Transparent;
            this.btncontrast.Image = ((System.Drawing.Image)(resources.GetObject("btncontrast.Image")));
            this.btncontrast.ImageActive = null;
            this.btncontrast.Location = new System.Drawing.Point(27, 528);
            this.btncontrast.Name = "btncontrast";
            this.btncontrast.Size = new System.Drawing.Size(57, 52);
            this.btncontrast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btncontrast.TabIndex = 4;
            this.btncontrast.TabStop = false;
            this.btncontrast.Zoom = 10;
            this.btncontrast.Click += new System.EventHandler(this.btncontrast_Click);
            // 
            // btnsharpness
            // 
            this.btnsharpness.BackColor = System.Drawing.Color.Transparent;
            this.btnsharpness.Image = ((System.Drawing.Image)(resources.GetObject("btnsharpness.Image")));
            this.btnsharpness.ImageActive = null;
            this.btnsharpness.Location = new System.Drawing.Point(27, 450);
            this.btnsharpness.Name = "btnsharpness";
            this.btnsharpness.Size = new System.Drawing.Size(57, 52);
            this.btnsharpness.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnsharpness.TabIndex = 4;
            this.btnsharpness.TabStop = false;
            this.btnsharpness.Zoom = 10;
            // 
            // btneffect
            // 
            this.btneffect.BackColor = System.Drawing.Color.Transparent;
            this.btneffect.Image = ((System.Drawing.Image)(resources.GetObject("btneffect.Image")));
            this.btneffect.ImageActive = null;
            this.btneffect.Location = new System.Drawing.Point(27, 361);
            this.btneffect.Name = "btneffect";
            this.btneffect.Size = new System.Drawing.Size(57, 52);
            this.btneffect.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btneffect.TabIndex = 4;
            this.btneffect.TabStop = false;
            this.btneffect.Zoom = 10;
            this.btneffect.Click += new System.EventHandler(this.btneffect_Click);
            // 
            // btncrop
            // 
            this.btncrop.BackColor = System.Drawing.Color.Transparent;
            this.btncrop.Image = ((System.Drawing.Image)(resources.GetObject("btncrop.Image")));
            this.btncrop.ImageActive = null;
            this.btncrop.Location = new System.Drawing.Point(27, 272);
            this.btncrop.Name = "btncrop";
            this.btncrop.Size = new System.Drawing.Size(57, 52);
            this.btncrop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btncrop.TabIndex = 4;
            this.btncrop.TabStop = false;
            this.btncrop.Zoom = 10;
            this.btncrop.Click += new System.EventHandler(this.btncrop_Click);
            // 
            // btnaddjustments
            // 
            this.btnaddjustments.BackColor = System.Drawing.Color.Transparent;
            this.btnaddjustments.Image = ((System.Drawing.Image)(resources.GetObject("btnaddjustments.Image")));
            this.btnaddjustments.ImageActive = null;
            this.btnaddjustments.Location = new System.Drawing.Point(27, 173);
            this.btnaddjustments.Name = "btnaddjustments";
            this.btnaddjustments.Size = new System.Drawing.Size(57, 52);
            this.btnaddjustments.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnaddjustments.TabIndex = 4;
            this.btnaddjustments.TabStop = false;
            this.btnaddjustments.Zoom = 10;
            this.btnaddjustments.Click += new System.EventHandler(this.btnaddjustments_Click);
            // 
            // btnbrightness
            // 
            this.btnbrightness.BackColor = System.Drawing.Color.Transparent;
            this.btnbrightness.Image = ((System.Drawing.Image)(resources.GetObject("btnbrightness.Image")));
            this.btnbrightness.ImageActive = null;
            this.btnbrightness.Location = new System.Drawing.Point(27, 82);
            this.btnbrightness.Name = "btnbrightness";
            this.btnbrightness.Size = new System.Drawing.Size(57, 52);
            this.btnbrightness.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnbrightness.TabIndex = 4;
            this.btnbrightness.TabStop = false;
            this.btnbrightness.Zoom = 10;
            this.btnbrightness.Click += new System.EventHandler(this.btnbrightness_Click);
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1267, 721);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(64, 64);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bunifuImageButton2.TabIndex = 1;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.Color.Transparent;
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageActive = null;
            this.btnsave.Location = new System.Drawing.Point(1398, 722);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(64, 64);
            this.btnsave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnsave.TabIndex = 1;
            this.btnsave.TabStop = false;
            this.btnsave.Zoom = 10;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(655, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 45);
            this.label2.TabIndex = 4;
            this.label2.Text = "Image Editor";
            // 
            // btnZoomIN
            // 
            this.btnZoomIN.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomIN.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomIN.Image")));
            this.btnZoomIN.ImageActive = null;
            this.btnZoomIN.Location = new System.Drawing.Point(167, 710);
            this.btnZoomIN.Name = "btnZoomIN";
            this.btnZoomIN.Size = new System.Drawing.Size(54, 52);
            this.btnZoomIN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnZoomIN.TabIndex = 4;
            this.btnZoomIN.TabStop = false;
            this.btnZoomIN.Zoom = 10;
            this.btnZoomIN.Click += new System.EventHandler(this.btnZoomIN_Click);
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomOut.Image")));
            this.btnZoomOut.ImageActive = null;
            this.btnZoomOut.Location = new System.Drawing.Point(227, 710);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(48, 52);
            this.btnZoomOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnZoomOut.TabIndex = 4;
            this.btnZoomOut.TabStop = false;
            this.btnZoomOut.Zoom = 10;
            this.btnZoomOut.Click += new System.EventHandler(this.btnZoomOut_Click);
            // 
            // panelBrightness
            // 
            this.panelBrightness.Controls.Add(this.lblshwbrightnessper);
            this.panelBrightness.Controls.Add(this.btnBright);
            this.panelBrightness.Controls.Add(this.brightnessTrackBar);
            this.panelBrightness.Controls.Add(this.brightnessBox);
            this.panelBrightness.Controls.Add(this.label6);
            this.panelBrightness.Location = new System.Drawing.Point(1119, 94);
            this.panelBrightness.Name = "panelBrightness";
            this.panelBrightness.Size = new System.Drawing.Size(343, 616);
            this.panelBrightness.TabIndex = 5;
            // 
            // lblshwbrightnessper
            // 
            this.lblshwbrightnessper.AutoSize = true;
            this.lblshwbrightnessper.BackColor = System.Drawing.Color.Transparent;
            this.lblshwbrightnessper.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblshwbrightnessper.ForeColor = System.Drawing.Color.Snow;
            this.lblshwbrightnessper.Location = new System.Drawing.Point(179, 53);
            this.lblshwbrightnessper.Name = "lblshwbrightnessper";
            this.lblshwbrightnessper.Size = new System.Drawing.Size(61, 21);
            this.lblshwbrightnessper.TabIndex = 28;
            this.lblshwbrightnessper.Text = "label14";
            // 
            // btnBright
            // 
            this.btnBright.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnBright.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnBright.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBright.BorderRadius = 0;
            this.btnBright.ButtonText = "APPLY";
            this.btnBright.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBright.DisabledColor = System.Drawing.Color.Gray;
            this.btnBright.Iconcolor = System.Drawing.Color.Transparent;
            this.btnBright.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnBright.Iconimage")));
            this.btnBright.Iconimage_right = null;
            this.btnBright.Iconimage_right_Selected = null;
            this.btnBright.Iconimage_Selected = null;
            this.btnBright.IconMarginLeft = 0;
            this.btnBright.IconMarginRight = 0;
            this.btnBright.IconRightVisible = true;
            this.btnBright.IconRightZoom = 0D;
            this.btnBright.IconVisible = true;
            this.btnBright.IconZoom = 90D;
            this.btnBright.IsTab = false;
            this.btnBright.Location = new System.Drawing.Point(108, 190);
            this.btnBright.Name = "btnBright";
            this.btnBright.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnBright.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnBright.OnHoverTextColor = System.Drawing.Color.White;
            this.btnBright.selected = false;
            this.btnBright.Size = new System.Drawing.Size(114, 49);
            this.btnBright.TabIndex = 27;
            this.btnBright.Text = "APPLY";
            this.btnBright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBright.Textcolor = System.Drawing.Color.White;
            this.btnBright.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBright.Click += new System.EventHandler(this.btnBright_Click);
            // 
            // brightnessTrackBar
            // 
            this.brightnessTrackBar.Location = new System.Drawing.Point(10, 107);
            this.brightnessTrackBar.Maximum = 1000;
            this.brightnessTrackBar.Minimum = -1000;
            this.brightnessTrackBar.Name = "brightnessTrackBar";
            this.brightnessTrackBar.Size = new System.Drawing.Size(330, 45);
            this.brightnessTrackBar.TabIndex = 26;
            this.brightnessTrackBar.TickFrequency = 50;
            this.brightnessTrackBar.Scroll += new System.EventHandler(this.brightnessTrackBar_Scroll);
            this.brightnessTrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.brightnessTrackBar_MouseUp);
            // 
            // brightnessBox
            // 
            this.brightnessBox.Location = new System.Drawing.Point(262, 54);
            this.brightnessBox.Name = "brightnessBox";
            this.brightnessBox.Size = new System.Drawing.Size(50, 20);
            this.brightnessBox.TabIndex = 25;
            this.brightnessBox.Visible = false;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(17, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(178, 23);
            this.label6.TabIndex = 24;
            this.label6.Text = "Adjust brightness by:";
            // 
            // effectpanel
            // 
            this.effectpanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.effectpanel.Controls.Add(this.bunifuImageButton10);
            this.effectpanel.Controls.Add(this.btnpixellate);
            this.effectpanel.Controls.Add(this.bunifuImageButton13);
            this.effectpanel.Controls.Add(this.bunifuImageButton12);
            this.effectpanel.Controls.Add(this.btninvert);
            this.effectpanel.Controls.Add(this.btnblacknwhite);
            this.effectpanel.Controls.Add(this.bunifuImageButton11);
            this.effectpanel.Controls.Add(this.btnjitter);
            this.effectpanel.Controls.Add(this.btnsepia);
            this.effectpanel.Location = new System.Drawing.Point(1116, 94);
            this.effectpanel.Name = "effectpanel";
            this.effectpanel.Size = new System.Drawing.Size(343, 619);
            this.effectpanel.TabIndex = 8;
            // 
            // bunifuImageButton10
            // 
            this.bunifuImageButton10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuImageButton10.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton10.Image")));
            this.bunifuImageButton10.ImageActive = null;
            this.bunifuImageButton10.Location = new System.Drawing.Point(243, 176);
            this.bunifuImageButton10.Name = "bunifuImageButton10";
            this.bunifuImageButton10.Size = new System.Drawing.Size(86, 87);
            this.bunifuImageButton10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton10.TabIndex = 0;
            this.bunifuImageButton10.TabStop = false;
            this.bunifuImageButton10.Zoom = 10;
            this.bunifuImageButton10.Click += new System.EventHandler(this.btnsepia_Click);
            // 
            // btnpixellate
            // 
            this.btnpixellate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnpixellate.Image = ((System.Drawing.Image)(resources.GetObject("btnpixellate.Image")));
            this.btnpixellate.ImageActive = null;
            this.btnpixellate.Location = new System.Drawing.Point(243, 44);
            this.btnpixellate.Name = "btnpixellate";
            this.btnpixellate.Size = new System.Drawing.Size(86, 87);
            this.btnpixellate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnpixellate.TabIndex = 0;
            this.btnpixellate.TabStop = false;
            this.btnpixellate.Zoom = 10;
            this.btnpixellate.Click += new System.EventHandler(this.btnpixellate_Click);
            // 
            // bunifuImageButton13
            // 
            this.bunifuImageButton13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuImageButton13.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton13.Image")));
            this.bunifuImageButton13.ImageActive = null;
            this.bunifuImageButton13.Location = new System.Drawing.Point(243, 304);
            this.bunifuImageButton13.Name = "bunifuImageButton13";
            this.bunifuImageButton13.Size = new System.Drawing.Size(86, 87);
            this.bunifuImageButton13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton13.TabIndex = 0;
            this.bunifuImageButton13.TabStop = false;
            this.bunifuImageButton13.Zoom = 10;
            this.bunifuImageButton13.Click += new System.EventHandler(this.btnsepia_Click);
            // 
            // bunifuImageButton12
            // 
            this.bunifuImageButton12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuImageButton12.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton12.Image")));
            this.bunifuImageButton12.ImageActive = null;
            this.bunifuImageButton12.Location = new System.Drawing.Point(131, 304);
            this.bunifuImageButton12.Name = "bunifuImageButton12";
            this.bunifuImageButton12.Size = new System.Drawing.Size(86, 87);
            this.bunifuImageButton12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton12.TabIndex = 0;
            this.bunifuImageButton12.TabStop = false;
            this.bunifuImageButton12.Zoom = 10;
            this.bunifuImageButton12.Click += new System.EventHandler(this.btnsepia_Click);
            // 
            // btninvert
            // 
            this.btninvert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btninvert.Image = ((System.Drawing.Image)(resources.GetObject("btninvert.Image")));
            this.btninvert.ImageActive = null;
            this.btninvert.Location = new System.Drawing.Point(131, 176);
            this.btninvert.Name = "btninvert";
            this.btninvert.Size = new System.Drawing.Size(86, 87);
            this.btninvert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btninvert.TabIndex = 0;
            this.btninvert.TabStop = false;
            this.btninvert.Zoom = 10;
            this.btninvert.Click += new System.EventHandler(this.btninvert_Click);
            // 
            // btnblacknwhite
            // 
            this.btnblacknwhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnblacknwhite.Image = ((System.Drawing.Image)(resources.GetObject("btnblacknwhite.Image")));
            this.btnblacknwhite.ImageActive = null;
            this.btnblacknwhite.Location = new System.Drawing.Point(131, 44);
            this.btnblacknwhite.Name = "btnblacknwhite";
            this.btnblacknwhite.Size = new System.Drawing.Size(86, 87);
            this.btnblacknwhite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnblacknwhite.TabIndex = 0;
            this.btnblacknwhite.TabStop = false;
            this.btnblacknwhite.Zoom = 10;
            this.btnblacknwhite.Click += new System.EventHandler(this.btnblacknwhite_Click);
            // 
            // bunifuImageButton11
            // 
            this.bunifuImageButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuImageButton11.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton11.Image")));
            this.bunifuImageButton11.ImageActive = null;
            this.bunifuImageButton11.Location = new System.Drawing.Point(15, 304);
            this.bunifuImageButton11.Name = "bunifuImageButton11";
            this.bunifuImageButton11.Size = new System.Drawing.Size(86, 87);
            this.bunifuImageButton11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton11.TabIndex = 0;
            this.bunifuImageButton11.TabStop = false;
            this.bunifuImageButton11.Zoom = 10;
            this.bunifuImageButton11.Click += new System.EventHandler(this.btnsepia_Click);
            // 
            // btnjitter
            // 
            this.btnjitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnjitter.Image = ((System.Drawing.Image)(resources.GetObject("btnjitter.Image")));
            this.btnjitter.ImageActive = null;
            this.btnjitter.Location = new System.Drawing.Point(15, 178);
            this.btnjitter.Name = "btnjitter";
            this.btnjitter.Size = new System.Drawing.Size(86, 87);
            this.btnjitter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnjitter.TabIndex = 0;
            this.btnjitter.TabStop = false;
            this.btnjitter.Zoom = 10;
            this.btnjitter.Click += new System.EventHandler(this.btnsepia_Click);
            // 
            // btnsepia
            // 
            this.btnsepia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnsepia.Image = ((System.Drawing.Image)(resources.GetObject("btnsepia.Image")));
            this.btnsepia.ImageActive = null;
            this.btnsepia.Location = new System.Drawing.Point(15, 46);
            this.btnsepia.Name = "btnsepia";
            this.btnsepia.Size = new System.Drawing.Size(86, 87);
            this.btnsepia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnsepia.TabIndex = 0;
            this.btnsepia.TabStop = false;
            this.btnsepia.Zoom = 10;
            this.btnsepia.Click += new System.EventHandler(this.btnsepia_Click);
            // 
            // panelrgb
            // 
            this.panelrgb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelrgb.Controls.Add(this.lblblue);
            this.panelrgb.Controls.Add(this.lblgreen);
            this.panelrgb.Controls.Add(this.lblredvalue);
            this.panelrgb.Controls.Add(this.trackbarblue);
            this.panelrgb.Controls.Add(this.trackbargreen);
            this.panelrgb.Controls.Add(this.label13);
            this.panelrgb.Controls.Add(this.trackbarred);
            this.panelrgb.Controls.Add(this.label12);
            this.panelrgb.Controls.Add(this.label8);
            this.panelrgb.Controls.Add(this.label9);
            this.panelrgb.Location = new System.Drawing.Point(1116, 94);
            this.panelrgb.Name = "panelrgb";
            this.panelrgb.Size = new System.Drawing.Size(346, 616);
            this.panelrgb.TabIndex = 29;
            // 
            // lblblue
            // 
            this.lblblue.AutoSize = true;
            this.lblblue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblblue.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblblue.Location = new System.Drawing.Point(251, 334);
            this.lblblue.Name = "lblblue";
            this.lblblue.Size = new System.Drawing.Size(15, 16);
            this.lblblue.TabIndex = 6;
            this.lblblue.Text = "0";
            // 
            // lblgreen
            // 
            this.lblgreen.AutoSize = true;
            this.lblgreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgreen.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblgreen.Location = new System.Drawing.Point(251, 220);
            this.lblgreen.Name = "lblgreen";
            this.lblgreen.Size = new System.Drawing.Size(15, 16);
            this.lblgreen.TabIndex = 6;
            this.lblgreen.Text = "0";
            // 
            // lblredvalue
            // 
            this.lblredvalue.AutoSize = true;
            this.lblredvalue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblredvalue.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblredvalue.Location = new System.Drawing.Point(251, 110);
            this.lblredvalue.Name = "lblredvalue";
            this.lblredvalue.Size = new System.Drawing.Size(15, 16);
            this.lblredvalue.TabIndex = 6;
            this.lblredvalue.Text = "0";
            // 
            // trackbarblue
            // 
            this.trackbarblue.Location = new System.Drawing.Point(15, 334);
            this.trackbarblue.Maximum = 20;
            this.trackbarblue.Name = "trackbarblue";
            this.trackbarblue.Size = new System.Drawing.Size(218, 45);
            this.trackbarblue.TabIndex = 0;
            this.trackbarblue.Scroll += new System.EventHandler(this.trackbarblue_Scroll);
            // 
            // trackbargreen
            // 
            this.trackbargreen.Location = new System.Drawing.Point(15, 220);
            this.trackbargreen.Maximum = 20;
            this.trackbargreen.Name = "trackbargreen";
            this.trackbargreen.Size = new System.Drawing.Size(218, 45);
            this.trackbargreen.TabIndex = 0;
            this.trackbargreen.Scroll += new System.EventHandler(this.trackbargreen_Scroll);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(23, 279);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 20);
            this.label13.TabIndex = 5;
            this.label13.Text = "Blue";
            // 
            // trackbarred
            // 
            this.trackbarred.Location = new System.Drawing.Point(12, 110);
            this.trackbarred.Maximum = 20;
            this.trackbarred.Name = "trackbarred";
            this.trackbarred.Size = new System.Drawing.Size(218, 45);
            this.trackbarred.TabIndex = 0;
            this.trackbarred.Scroll += new System.EventHandler(this.trackbarred_Scroll_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(20, 168);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 20);
            this.label12.TabIndex = 5;
            this.label12.Text = "Green";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(104, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 20);
            this.label8.TabIndex = 5;
            this.label8.Text = "Adjust";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(20, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "Red";
            // 
            // bunifuImageButton7
            // 
            this.bunifuImageButton7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton7.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton7.Image")));
            this.bunifuImageButton7.ImageActive = null;
            this.bunifuImageButton7.Location = new System.Drawing.Point(581, 710);
            this.bunifuImageButton7.Name = "bunifuImageButton7";
            this.bunifuImageButton7.Size = new System.Drawing.Size(48, 52);
            this.bunifuImageButton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bunifuImageButton7.TabIndex = 4;
            this.bunifuImageButton7.TabStop = false;
            this.bunifuImageButton7.Zoom = 10;
            this.bunifuImageButton7.Click += new System.EventHandler(this.bunifuImageButton7_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(543, 766);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 20);
            this.label16.TabIndex = 5;
            this.label16.Text = "FIT TO SCREEN";
            // 
            // BTNUNDO
            // 
            this.BTNUNDO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNUNDO.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BTNUNDO.Location = new System.Drawing.Point(823, 707);
            this.BTNUNDO.Name = "BTNUNDO";
            this.BTNUNDO.Size = new System.Drawing.Size(127, 40);
            this.BTNUNDO.TabIndex = 30;
            this.BTNUNDO.Text = "UNDO";
            this.BTNUNDO.UseVisualStyleBackColor = true;
            this.BTNUNDO.Click += new System.EventHandler(this.BTNUNDO_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.btnapplyhue);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(1479, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(330, 619);
            this.panel2.TabIndex = 9;
            // 
            // btnapplyhue
            // 
            this.btnapplyhue.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnapplyhue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnapplyhue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnapplyhue.BorderRadius = 0;
            this.btnapplyhue.ButtonText = "APPLY";
            this.btnapplyhue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnapplyhue.DisabledColor = System.Drawing.Color.Gray;
            this.btnapplyhue.Iconcolor = System.Drawing.Color.Transparent;
            this.btnapplyhue.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnapplyhue.Iconimage")));
            this.btnapplyhue.Iconimage_right = null;
            this.btnapplyhue.Iconimage_right_Selected = null;
            this.btnapplyhue.Iconimage_Selected = null;
            this.btnapplyhue.IconMarginLeft = 0;
            this.btnapplyhue.IconMarginRight = 0;
            this.btnapplyhue.IconRightVisible = true;
            this.btnapplyhue.IconRightZoom = 0D;
            this.btnapplyhue.IconVisible = true;
            this.btnapplyhue.IconZoom = 90D;
            this.btnapplyhue.IsTab = false;
            this.btnapplyhue.Location = new System.Drawing.Point(115, 327);
            this.btnapplyhue.Name = "btnapplyhue";
            this.btnapplyhue.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnapplyhue.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnapplyhue.OnHoverTextColor = System.Drawing.Color.White;
            this.btnapplyhue.selected = false;
            this.btnapplyhue.Size = new System.Drawing.Size(114, 49);
            this.btnapplyhue.TabIndex = 28;
            this.btnapplyhue.Text = "APPLY";
            this.btnapplyhue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnapplyhue.Textcolor = System.Drawing.Color.White;
            this.btnapplyhue.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnapplyhue.Click += new System.EventHandler(this.btnapplyhue_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.huePicker1);
            this.groupBox1.Controls.Add(this.hueBox);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(21, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(291, 250);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Hue Value";
            // 
            // hueBox
            // 
            this.hueBox.Location = new System.Drawing.Point(121, 27);
            this.hueBox.Name = "hueBox";
            this.hueBox.Size = new System.Drawing.Size(100, 29);
            this.hueBox.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(24, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 36);
            this.label18.TabIndex = 0;
            this.label18.Text = "&Hue:";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(155, 31);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(155, 31);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(1646, 722);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(163, 65);
            this.tabControl1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(913, 601);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(159, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(924, 616);
            this.panel1.TabIndex = 6;
            // 
            // btnREDO
            // 
            this.btnREDO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnREDO.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnREDO.Location = new System.Drawing.Point(956, 707);
            this.btnREDO.Name = "btnREDO";
            this.btnREDO.Size = new System.Drawing.Size(127, 40);
            this.btnREDO.TabIndex = 30;
            this.btnREDO.Text = "REDO";
            this.btnREDO.UseVisualStyleBackColor = true;
            this.btnREDO.Click += new System.EventHandler(this.btnREDO_Click);
            // 
            // panelcontrast
            // 
            this.panelcontrast.Controls.Add(this.lblcontrastper);
            this.panelcontrast.Controls.Add(this.btnapplyContrast);
            this.panelcontrast.Controls.Add(this.trackbarcontrast);
            this.panelcontrast.Controls.Add(this.label14);
            this.panelcontrast.Controls.Add(this.label15);
            this.panelcontrast.Location = new System.Drawing.Point(1119, 94);
            this.panelcontrast.Name = "panelcontrast";
            this.panelcontrast.Size = new System.Drawing.Size(343, 616);
            this.panelcontrast.TabIndex = 5;
            // 
            // lblcontrastper
            // 
            this.lblcontrastper.AutoSize = true;
            this.lblcontrastper.BackColor = System.Drawing.Color.Transparent;
            this.lblcontrastper.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcontrastper.ForeColor = System.Drawing.Color.Snow;
            this.lblcontrastper.Location = new System.Drawing.Point(181, 103);
            this.lblcontrastper.Name = "lblcontrastper";
            this.lblcontrastper.Size = new System.Drawing.Size(0, 21);
            this.lblcontrastper.TabIndex = 28;
            // 
            // btnapplyContrast
            // 
            this.btnapplyContrast.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnapplyContrast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnapplyContrast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnapplyContrast.BorderRadius = 0;
            this.btnapplyContrast.ButtonText = "APPLY";
            this.btnapplyContrast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnapplyContrast.DisabledColor = System.Drawing.Color.Gray;
            this.btnapplyContrast.Iconcolor = System.Drawing.Color.Transparent;
            this.btnapplyContrast.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnapplyContrast.Iconimage")));
            this.btnapplyContrast.Iconimage_right = null;
            this.btnapplyContrast.Iconimage_right_Selected = null;
            this.btnapplyContrast.Iconimage_Selected = null;
            this.btnapplyContrast.IconMarginLeft = 0;
            this.btnapplyContrast.IconMarginRight = 0;
            this.btnapplyContrast.IconRightVisible = true;
            this.btnapplyContrast.IconRightZoom = 0D;
            this.btnapplyContrast.IconVisible = true;
            this.btnapplyContrast.IconZoom = 90D;
            this.btnapplyContrast.IsTab = false;
            this.btnapplyContrast.Location = new System.Drawing.Point(120, 267);
            this.btnapplyContrast.Name = "btnapplyContrast";
            this.btnapplyContrast.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnapplyContrast.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnapplyContrast.OnHoverTextColor = System.Drawing.Color.White;
            this.btnapplyContrast.selected = false;
            this.btnapplyContrast.Size = new System.Drawing.Size(114, 49);
            this.btnapplyContrast.TabIndex = 27;
            this.btnapplyContrast.Text = "APPLY";
            this.btnapplyContrast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnapplyContrast.Textcolor = System.Drawing.Color.White;
            this.btnapplyContrast.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnapplyContrast.Click += new System.EventHandler(this.btnapplyContrast_Click);
            // 
            // trackbarcontrast
            // 
            this.trackbarcontrast.Location = new System.Drawing.Point(13, 172);
            this.trackbarcontrast.Maximum = 5000;
            this.trackbarcontrast.Minimum = -1000;
            this.trackbarcontrast.Name = "trackbarcontrast";
            this.trackbarcontrast.Size = new System.Drawing.Size(330, 45);
            this.trackbarcontrast.TabIndex = 26;
            this.trackbarcontrast.TickFrequency = 200;
            this.trackbarcontrast.Value = 1;
            this.trackbarcontrast.Scroll += new System.EventHandler(this.trackbarcontrast_Scroll);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(17, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(178, 23);
            this.label14.TabIndex = 24;
            this.label14.Text = " CONTRAST  FACTOR :";
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(85, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 23);
            this.label15.TabIndex = 24;
            this.label15.Text = "ADJUST CONTRAST ";
            // 
            // panelSaturation
            // 
            this.panelSaturation.Controls.Add(this.txtsaturationvalue);
            this.panelSaturation.Controls.Add(this.lblsaturationvalue);
            this.panelSaturation.Controls.Add(this.btnsaturation);
            this.panelSaturation.Controls.Add(this.trackbarsaturation);
            this.panelSaturation.Controls.Add(this.label20);
            this.panelSaturation.Controls.Add(this.label21);
            this.panelSaturation.Location = new System.Drawing.Point(1113, 94);
            this.panelSaturation.Name = "panelSaturation";
            this.panelSaturation.Size = new System.Drawing.Size(343, 616);
            this.panelSaturation.TabIndex = 29;
            // 
            // txtsaturationvalue
            // 
            this.txtsaturationvalue.Location = new System.Drawing.Point(305, 79);
            this.txtsaturationvalue.Name = "txtsaturationvalue";
            this.txtsaturationvalue.Size = new System.Drawing.Size(13, 20);
            this.txtsaturationvalue.TabIndex = 29;
            this.txtsaturationvalue.Visible = false;
            // 
            // lblsaturationvalue
            // 
            this.lblsaturationvalue.AutoSize = true;
            this.lblsaturationvalue.BackColor = System.Drawing.Color.Transparent;
            this.lblsaturationvalue.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsaturationvalue.ForeColor = System.Drawing.Color.Snow;
            this.lblsaturationvalue.Location = new System.Drawing.Point(150, 103);
            this.lblsaturationvalue.Name = "lblsaturationvalue";
            this.lblsaturationvalue.Size = new System.Drawing.Size(0, 21);
            this.lblsaturationvalue.TabIndex = 28;
            // 
            // btnsaturation
            // 
            this.btnsaturation.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnsaturation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnsaturation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsaturation.BorderRadius = 0;
            this.btnsaturation.ButtonText = "APPLY";
            this.btnsaturation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsaturation.DisabledColor = System.Drawing.Color.Gray;
            this.btnsaturation.Iconcolor = System.Drawing.Color.Transparent;
            this.btnsaturation.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnsaturation.Iconimage")));
            this.btnsaturation.Iconimage_right = null;
            this.btnsaturation.Iconimage_right_Selected = null;
            this.btnsaturation.Iconimage_Selected = null;
            this.btnsaturation.IconMarginLeft = 0;
            this.btnsaturation.IconMarginRight = 0;
            this.btnsaturation.IconRightVisible = true;
            this.btnsaturation.IconRightZoom = 0D;
            this.btnsaturation.IconVisible = true;
            this.btnsaturation.IconZoom = 90D;
            this.btnsaturation.IsTab = false;
            this.btnsaturation.Location = new System.Drawing.Point(120, 267);
            this.btnsaturation.Name = "btnsaturation";
            this.btnsaturation.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnsaturation.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnsaturation.OnHoverTextColor = System.Drawing.Color.White;
            this.btnsaturation.selected = false;
            this.btnsaturation.Size = new System.Drawing.Size(114, 49);
            this.btnsaturation.TabIndex = 27;
            this.btnsaturation.Text = "APPLY";
            this.btnsaturation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsaturation.Textcolor = System.Drawing.Color.White;
            this.btnsaturation.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsaturation.Click += new System.EventHandler(this.btnsaturation_Click);
            // 
            // trackbarsaturation
            // 
            this.trackbarsaturation.Location = new System.Drawing.Point(13, 172);
            this.trackbarsaturation.Maximum = 1000;
            this.trackbarsaturation.Minimum = -1000;
            this.trackbarsaturation.Name = "trackbarsaturation";
            this.trackbarsaturation.Size = new System.Drawing.Size(330, 45);
            this.trackbarsaturation.TabIndex = 26;
            this.trackbarsaturation.TickFrequency = 50;
            this.trackbarsaturation.Value = 1;
            this.trackbarsaturation.Scroll += new System.EventHandler(this.trackbarsaturation_Scroll);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label20.Location = new System.Drawing.Point(23, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 23);
            this.label20.TabIndex = 24;
            this.label20.Text = "ADJUST BY ";
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label21.Location = new System.Drawing.Point(110, 32);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(178, 23);
            this.label21.TabIndex = 24;
            this.label21.Text = "SATURATION";
            // 
            // huePicker1
            // 
            this.huePicker1.Location = new System.Drawing.Point(64, 75);
            this.huePicker1.Name = "huePicker1";
            this.huePicker1.Size = new System.Drawing.Size(160, 157);
            this.huePicker1.TabIndex = 0;
            this.huePicker1.Text = "huePicker1";
            this.huePicker1.ValuesChanged += new System.EventHandler(this.huePicker1_ValuesChanged);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1821, 851);
            this.Controls.Add(this.panelSaturation);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelcontrast);
            this.Controls.Add(this.panelBrightness);
            this.Controls.Add(this.panelrgb);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnREDO);
            this.Controls.Add(this.BTNUNDO);
            this.Controls.Add(this.effectpanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.bunifuImageButton2);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.bunifuImageButton7);
            this.Controls.Add(this.btnZoomOut);
            this.Controls.Add(this.btnZoomIN);
            this.Controls.Add(this.bunifuImageButton1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncontrast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsharpness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btneffect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncrop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnaddjustments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnbrightness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnZoomIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnZoomOut)).EndInit();
            this.panelBrightness.ResumeLayout(false);
            this.panelBrightness.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).EndInit();
            this.effectpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnpixellate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btninvert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnblacknwhite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnjitter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsepia)).EndInit();
            this.panelrgb.ResumeLayout(false);
            this.panelrgb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarblue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbargreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton7)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelcontrast.ResumeLayout(false);
            this.panelcontrast.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarcontrast)).EndInit();
            this.panelSaturation.ResumeLayout(false);
            this.panelSaturation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarsaturation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuImageButton btnsave;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuImageButton btneffect;
        private Bunifu.Framework.UI.BunifuImageButton btncrop;
        private Bunifu.Framework.UI.BunifuImageButton btnaddjustments;
        private Bunifu.Framework.UI.BunifuImageButton btnbrightness;
        private Bunifu.Framework.UI.BunifuImageButton btnZoomOut;
        private Bunifu.Framework.UI.BunifuImageButton btnZoomIN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuImageButton btncontrast;
        private Bunifu.Framework.UI.BunifuImageButton btnsharpness;
        private System.Windows.Forms.Panel panelBrightness;
        private System.Windows.Forms.TrackBar brightnessTrackBar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelrgb;
        private System.Windows.Forms.TrackBar trackbarblue;
        private System.Windows.Forms.TrackBar trackbargreen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar trackbarred;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton7;
        private System.Windows.Forms.Panel effectpanel;
        private Bunifu.Framework.UI.BunifuImageButton btnsepia;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton10;
        private Bunifu.Framework.UI.BunifuImageButton btnpixellate;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton13;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton12;
        private Bunifu.Framework.UI.BunifuImageButton btninvert;
        private Bunifu.Framework.UI.BunifuImageButton btnblacknwhite;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton11;
        private Bunifu.Framework.UI.BunifuImageButton btnjitter;
        private System.Windows.Forms.Label lblredvalue;
        private System.Windows.Forms.Button BTNUNDO;
        private System.Windows.Forms.Label lblblue;
        private System.Windows.Forms.Label lblgreen;
        public System.Windows.Forms.TextBox brightnessBox;
        private Bunifu.Framework.UI.BunifuFlatButton btnBright;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox hueBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Control.HuePicker huePicker1;
        private System.Windows.Forms.Button btnREDO;
        private Bunifu.Framework.UI.BunifuFlatButton btnapplyhue;
        private System.Windows.Forms.Label lblshwbrightnessper;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelcontrast;
        private System.Windows.Forms.Label lblcontrastper;
        private Bunifu.Framework.UI.BunifuFlatButton btnapplyContrast;
        private System.Windows.Forms.TrackBar trackbarcontrast;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panelSaturation;
        private System.Windows.Forms.TextBox txtsaturationvalue;
        private System.Windows.Forms.Label lblsaturationvalue;
        private Bunifu.Framework.UI.BunifuFlatButton btnsaturation;
        private System.Windows.Forms.TrackBar trackbarsaturation;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
    }
}

