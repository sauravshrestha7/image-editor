﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
namespace Image_Editor
{
    public class colorbalance
    {
        public Bitmap RGB(Image getIMG,float red, float green, float blue)
        {

            Bitmap bmpInverted = new Bitmap(getIMG);   /* creating a bitmap of the height of imported picture in picturebox which consists of the pixel data for a graphics image
                 
                                                        and its attributes. A Bitmap is an object used to work with images defined by pixel data.*/
            float[][] colormatrixelements =      // now creating the color matrix object to change the colors or apply  image filter on image
               {
                    new float[]{1+red, 0, 0, 0, 0},
            new float[]{0, 1+green, 0, 0, 0},
            new float[]{0, 0, 1+blue, 0, 0},
            new float[]{0, 0, 0, 1, 0},
            new float[]{0, 0, 0, 0, 1}
                };

            //creating an object of imageattribute ia to change the attribute of images
            ColorMatrix cmPicture = new ColorMatrix(colormatrixelements);
            ImageAttributes ia = new ImageAttributes();

            ia.SetColorMatrix(cmPicture, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);           //pass the color matrix to imageattribute object ia

            Graphics g = default(Graphics);
            g = Graphics.FromImage(bmpInverted);   /*create a new object of graphics named g, ; Create graphics object for alteration.
                                                            Graphics newGraphics = Graphics.FromImage(imageFile); is the format of loading image into graphics for alteration*/

            g.DrawImage(bmpInverted, new Rectangle(0, 0, bmpInverted.Width + 1, bmpInverted.Height + 1), 0, 0, bmpInverted.Width, bmpInverted.Height, GraphicsUnit.Pixel, ia);



            /*   g.drawimage(image, new rectangle(location of rectangle axix-x, location axis-y, width of rectangle, height of rectangle),
           location of image in rectangle x-axis, location of image in rectangle y-axis, width of image, height of image,
           format of graphics unit,provide the image attributes   */



            //Releases all resources used by this Graphics.

           

            g.Dispose();
          
          

            return bmpInverted;
        }
    }
}
