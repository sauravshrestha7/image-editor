﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Editor
{
    public partial class zoom : Form
    {
        public zoom()
        {
            InitializeComponent();
        }
        Bitmap picture;
        Size picture_size;
        int zoom1 = 1;
        int limit = 128;

        private void button1_Click(object sender, EventArgs e)
        {
            picture = new Bitmap(pictureBox1.Image);
            picture_size = picture.Size;

            if (zoom1 < limit)
            {
                zoom1 = zoom1 * 2;
            }
            pictureBox1.Size = new Size(zoom1 * picture_size.Width, zoom1 * picture_size.Height);
            pictureBox1.Invalidate();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (zoom1 > 0)
            {
                zoom1 = zoom1 / 2;

            }

            pictureBox1.Size = new Size(zoom1 * picture_size.Width, zoom1 * picture_size.Height);
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (picture != null)
            {
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                e.Graphics.DrawImage(picture, 0, 0, pictureBox1.Width * zoom1, pictureBox1.Height * zoom1);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }




    }
}
